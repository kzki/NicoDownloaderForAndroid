package info.indexbook.nicodownloader;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Base64;
import android.util.Log;

import java.nio.ByteBuffer;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryptor {
    // key info
    private final int KEY_LENGTH = 16;
    private final int UUID_SIZE  = 36;
    private final String CHARCODE = "UTF-8";

    // encrypt key
    private String ENCRYPT_KEY = null;
    private String ENCRYPT_IV = null;

    // encrypt and decrypt string
    private String input = null;

    /**
     * Constructor
     * @param input
     */
    public Encryptor(String input) {
        this.input = input;
    }

    /**
     * create encrypt key and initialization vector
     * @param uuid
     * @return true or false
     */
    public boolean init(String uuid) {
        if(uuid.length() != UUID_SIZE) return false;

        // encrypt key
        int begin = 0;
        int end = KEY_LENGTH;
        ENCRYPT_KEY = uuid.substring(begin, end);

        // initialization vector
        begin = uuid.length() - KEY_LENGTH;
        end = uuid.length();
        ENCRYPT_IV = uuid.substring(begin, end);

        return true;
    }

    /**
     * encrypt
     * @return String
     */
    public String encrypt() {
        String result = null;

        try {
            byte[] byteInput = input.getBytes(CHARCODE);
            byte[] byteKey = ENCRYPT_KEY.getBytes(CHARCODE);
            byte[] byteIv = ENCRYPT_IV.getBytes(CHARCODE);

            SecretKeySpec key = new SecretKeySpec(byteKey, "AES");
            IvParameterSpec iv = new IvParameterSpec(byteIv);

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);

            byte[] byteResult = cipher.doFinal(byteInput);
            result = Base64.encodeToString(byteResult, Base64.DEFAULT);
            return result;
        }
        catch(Exception e) {
            Log.e("encrypt", "" + e);
            return null;
        }
    }

    /**
     * decrypt
     * @return String
     */
    public String decrypt() {
        String result = null;

        try {
            byte[] byteInput = Base64.decode(input, Base64.DEFAULT);
            byte[] byteKey = ENCRYPT_KEY.getBytes(CHARCODE);
            byte[] byteIv = ENCRYPT_IV.getBytes(CHARCODE);

            SecretKeySpec key = new SecretKeySpec(byteKey, "AES");
            IvParameterSpec iv = new IvParameterSpec(byteIv);

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key, iv);

            byte[] byteResult = cipher.doFinal(byteInput);
            result = new String(byteResult, CHARCODE);
            return result;
        }
        catch (Exception e) {
            Log.e("decrypt", "" + e);
            return null;
        }
    }

    /**
     * static encrpyt method
     * @param context, input
     * @return String
     */
    public static String encrypt(Context context, String input) {
        // create encrypt key
        try {
            PackageInfo info = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);

            long time = info.firstInstallTime;
            ByteBuffer buf = ByteBuffer.allocate(8);
            buf.putLong(time);
            byte[] name = buf.array();

            String uuid = UUID.nameUUIDFromBytes(name).toString();

            // encrypt
            Encryptor encryptor = new Encryptor(input);
            encryptor.init(uuid);
            String result = encryptor.encrypt();
            return result;
        }
        catch (Exception e) {
            Log.e("encrypt error", "" + e);
            return null;
        }
    }

    public static String decrypt(Context context, String input) {
        try {
            PackageInfo info = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);

            long time = info.firstInstallTime;
            ByteBuffer buf = ByteBuffer.allocate(8);
            buf.putLong(time);
            byte[] name = buf.array();

            String uuid = UUID.nameUUIDFromBytes(name).toString();

            // decrypt
            Encryptor encryptor = new Encryptor(input);
            encryptor.init(uuid);
            String result = encryptor.decrypt();
            return result;
        }
        catch(Exception e) {
            Log.e("decrypt error", "" + e);
            return null;
        }
    }
}
