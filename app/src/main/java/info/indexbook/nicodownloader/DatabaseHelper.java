package info.indexbook.nicodownloader;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    //
    public static final int DATABASE_VERSION = 1;
    // Database name
    public static final String DATABASE_NAME = "History.db";
    // Query for create database
    public static final String SQL_CREATE_ENTRIES =
            "create table history (" +
            "_id integer primary key, id text unique, " +
            "title text, directory text, " +
            "mode text, url text, " +
            "size text, type text" +
            ")";
    // Query for delete table
    private static final String SQL_DELETE_ENTRIES = "drop table history";

    /**
     * Constructor
     *
     * @param context
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}