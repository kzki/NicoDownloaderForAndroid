package info.indexbook.nicodownloader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class InfoActivity extends Activity {
    // values
    private String url;
    private ProgressDialog dialog;
    // nico api base url
    private String baseURL = "http://ext.nicovideo.jp/thumb/";

    /**
     * onCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // show WebView and Download button
        setContentView(R.layout.activity_info);

        // get url from intent
        url = getIntent().getStringExtra("EXTRA_URL");

        // create thumb url
        String thumb = baseURL + url.substring(url.lastIndexOf("watch") + 6);

        // get WebView from layout
        WebView webView = (WebView) this.findViewById(R.id.webView);

        // show dialog
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                dialog = ProgressDialog.show(InfoActivity.this, null, "Loading...");
                dialog.setCancelable(true);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
                super.onPageFinished(view, url);
            }
        });
        // load thumb page
        webView.loadUrl(thumb);
    }

    /**
     * start Download when push download button
     * @param view
     */
    public void startDownload(View view) {
        Intent intent = new Intent(getApplicationContext(), NicoDownloader.class);
        startService(intent);
    }
}
