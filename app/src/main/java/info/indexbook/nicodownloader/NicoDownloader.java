package info.indexbook.nicodownloader;


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class NicoDownloader extends Service implements Runnable{
    // login url
    public static final String LOGIN_URL = "https://secure.nicovideo.jp/secure/login?site=niconico";

    /**
     * onCreate
     */
    @Override
    public void onCreate() {
        Log.i("onCreate", "");
    }

    /**
     * onStartCommand
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int StartId) {
        Thread th = new Thread(this);
        th.start();
        return START_STICKY;
    }

    /**
     * onDestroy
     */
    public void onDestroy() {
    }

    /**
     * onBind
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * thread behavior
     */
    @Override
    public void run() {
        Log.i("Runnable", "");
        download();
    }

    /**
     * Download method
     */
    private void download() {
        try {
            // get mail and password from preference
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String mail = prefs.getString("pref_nico_mail", "");
            String pass = prefs.getString("pref_nico_pass", "");
            pass = Encryptor.decrypt(this, pass);

            URL url = new URL(LOGIN_URL);
            String postValue = "mail=" + mail + "&password=" + pass;
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setUseCaches(true);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.connect();
            OutputStream os = conn.getOutputStream();
            os.write(postValue.getBytes());
            os.flush();
            os.close();

            String path = Environment.getExternalStorageDirectory().getPath();
            FileOutputStream out = new FileOutputStream(path + "/Download/login.html");
            InputStream input = conn.getInputStream();

            while(true) {
                int read = input.read();
                if (read == -1) break;
                out.write(read);
            }
        }
        catch(Exception e) {
            Log.e("Exception" , "" + e);
        }
        Log.i("connectTest", "");
    }
}
