package info.indexbook.nicodownloader;


import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;

public class EncryptedEditTextPreference extends EditTextPreference {

    /**
     * Constructor
     */
    public EncryptedEditTextPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public EncryptedEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public EncryptedEditTextPreference(Context context) {
        super(context);
    }

    @Override
    public String getText() {
        String value = super.getText();
        return Encryptor.decrypt(getContext(), value);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        super.setText(restoreValue ? getPersistedString(null) : defaultValue.toString());
    }

    @Override
    public void setText(String text) {
        if(text.equals("")) {
            super.setText(null);
            return;
        }
        super.setText(Encryptor.encrypt(getContext(), text));
    }
}
