package info.indexbook.nicodownloader;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

public class SettingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // show content
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment()).commit();
    }

    public static class SettingsFragment extends PreferenceFragment implements
            SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // load layout of setting display
            addPreferencesFromResource(R.layout.activity_setting);

            // show warning when delete history
            Preference pref = findPreference("delete");
            pref.setOnPreferenceClickListener(getPrefClickListener());

            // refresh display
            resetSummary();
        }

        @Override
        public void onResume() {
            super.onResume();
            resetSummary();
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            resetSummary();
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(
                SharedPreferences sharedPreferences, String key) {
            resetSummary();
        }

        // set current value
        public void resetSummary() {
            EditTextPreference editTextPref = (EditTextPreference) getPreferenceScreen()
                    .findPreference("pref_save_directory");
            editTextPref.setSummary(editTextPref.getText());
        }

        /**
         * Get Preference Click Listener
         * @return Preference.OnPreferenceClickListener
         */
        private Preference.OnPreferenceClickListener getPrefClickListener() {
            return new Preference.OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    // delete history alert
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.delete_history).setMessage(R.string.delete_message)
                        // OK Button
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // open database
                                DatabaseHelper helper = new DatabaseHelper(getActivity());
                                SQLiteDatabase db = helper.getWritableDatabase();
                                // delete database
                                db.delete("history", null, null);
                            }
                        }).setNegativeButton("Cancel", null);

                    // show alert
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return false;
                }
            };
        }
    }
}
