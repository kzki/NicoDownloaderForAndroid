package info.indexbook.nicodownloader;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;

import java.io.File;


public class MainActivity extends ListActivity {
    // Action mode
    private MainActivity activity = this;
    // NICONICO base url
    private final String WATCH_URL = "http://sp.nicovideo.jp/watch/";

    /**
     * onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init save directory
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        if(prefs.getString("pref_save_directory", "").equals("")) {
            SharedPreferences.Editor editor = prefs.edit();
            String path = Environment.getExternalStorageDirectory().getPath();
            editor.putString("pref_save_directory", path + "/Download");
            editor.apply();
        }

        // when start other activity
        if (Intent.ACTION_SEND.equals(getIntent().getAction())) {
            String url = getIntent().getExtras()
                    .getCharSequence(Intent.EXTRA_TEXT).toString();
            url = url.replace("sp", "www");
            url = url.replaceAll("\\?.*", "");
            Intent intent = new Intent(getApplicationContext(), InfoActivity.class);
            intent.putExtra("EXTRA_URL", url);
            startActivity(intent);
        }

        if (Intent.ACTION_VIEW.equals(getIntent().getAction())) {
            String url = getIntent().getDataString();
            url = url.replace("sp", "www");
            url = url.replaceAll("\\?.*", "");
            Intent intent = new Intent(getApplicationContext(), InfoActivity.class);
            intent.putExtra("EXTRA_URL", url);
            startActivity(intent);
        }

        NicoDownloader nicoDownloader = new NicoDownloader();

    }

    /**
     * onResume
     */
    @Override
    protected void onResume() {
        super.onResume();
        createList();
    }

    /**
     *  Show menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Menu behavior
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * create download list
     */
    private void createList() {
        // open database
        DatabaseHelper helper = new DatabaseHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        // load database
        Cursor cursor = db.query("history", null, null, null, null, null, "_id DESC");

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                this,
                R.layout.row,
                cursor,
                new String[] {"id", "title", "mode", "size", "type"},
                new int[] {R.id.imageView, R.id.column_title, R.id.column_mode, R.id.column_size, R.id.column_type},
                0);

        // set thumb
        adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {

            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                if(columnIndex == 1) {
                    File file = new File(getFilesDir(), cursor.getString(columnIndex));
                    Uri uri = Uri.fromFile(file);
                    ((ImageView) view).setImageURI(uri);
                    return true;
                }
                return false;
            }
        });

        setListAdapter(adapter);
        db.close();
    }
}